import pytest
import random
import networkx as nx
from dag_part import partition, reassemble


def generate_random_dag(n, p, q=0.5):
    """Helper function to generate Erdős-Rényi directed acyclic graphs with random node feature `supported`

    Args:
    n (int): number of nodes
    p (float): probability of edge creation
    q (float): probability of supported

    Returns:
    A directed acyclic random graph
    """
    # Build a random directed graph and ensure it is acyclic
    _G = nx.erdos_renyi_graph(n, p, seed=None, directed=True)
    G = nx.DiGraph([(u, v) for (u, v) in _G.edges() if u < v])

    # Randomly assign supported or not to each node
    random_support = {v: {"supported": random.uniform(0, 1) < q} for v in G.nodes}
    nx.set_node_attributes(G, random_support)

    # Mark all source and sink nodes as auxiliary (input/output)
    sources = [v for v, indegree in G.in_degree(G.nodes) if indegree == 0]
    sinks = [v for v, outdegree in G.out_degree(G.nodes) if outdegree == 0]
    for v in sources + sinks:
        G.nodes[v]["supported"] = None

    return G


@pytest.mark.parametrize("n", (16, 64, 256))
@pytest.mark.parametrize("p", (0.1, 0.2, 0.3))
@pytest.mark.parametrize("q", (0.1, 0.5, 0.9))
def test_dag_part(n, p, q):
    # generate a random graph
    G = generate_random_dag(n, p, q)
    assert(nx.is_directed_acyclic_graph(G))

    # perform partition and reassemble on G
    SGs = partition(G)
    G_ = reassemble(SGs)

    # all subgraphs need to be either entirely supported or entirely unsupported
    for sg in SGs:
        s = [
            sg.nodes[v]["supported"]
            for v in sg.nodes
            if sg.nodes[v]["supported"] is not None
        ]
        assert(all(s) or not any(s))

    # reassembled graph needs to be isomorphic to the original graph
    assert(nx.is_isomorphic(G, G_))
    