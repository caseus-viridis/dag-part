import math
import matplotlib.pyplot as plt
import networkx as nx


def get_node_labels(G):
    "Returns a list of node labels"
    return {
        v: "-".join([str(_v) for _v in v]) if isinstance(v, tuple) else str(v)
        for v in G.nodes
    }


def get_node_colors(G):
    """Returns a list of node colors
    "red": not supported
    "green": supported
    "yellow": auxiliary
    """
    return [
        (
            "green"
            if G.nodes[v]["supported"]
            else "red"
            if G.nodes[v]["supported"] is not None
            else "yellow"
        )
        for v in G.nodes
    ]


def graph_plot(G, fname):
    "Generate graphs ans save to image file"
    if isinstance(G, nx.DiGraph):
        G = [G]
    num_graphs = len(G)
    grid = math.ceil(math.sqrt(num_graphs))
    _, ax = plt.subplots(nrows=grid, ncols=grid)
    if grid > 1:
        ax = ax.reshape(grid ** 2)
    else:
        ax = [ax]
    for i, g in enumerate(G):
        nx.draw_spring(
            g,
            k=5 / math.sqrt(g.order()),
            labels=get_node_labels(g),
            font_size=6,
            node_color=get_node_colors(g),
            ax=ax[i],
        )
    for a in ax:
        a.axis("off")
    plt.savefig(fname)
