# `dag_part`

## Problem

Given a directed acyclic graph containing both “supported” and “unsupported” nodes, write python code to partition the graph into a list of subgraphs that contain only entirely supported or entirely unsupported nodes. Also write the reverse transform, assembling a list of subgraphs that have been partitioned into a single graph that contains both supported and unsupported nodes. Represent the graph in any way you like. Along with the nodes and their connections, the input and output edges of the original DAG are known as well. Write unit tests.

## Solution

### Graph representation

We use `networkx` for graph data structure and basic graph algorithms.  

### Main functionalities

The two main functionalities are DAG partition into subgraphs, `dat_part.partition()`, and reassembling subgraphs back to DAG, `dat_part.reassemble()`.

### Tests

```sh
pytest test_dag_part.py
```

### Example

The example in the PDF problem sheet is recreated as:

```sh
python example.py
```

Images are generated as in the problem sheet:  

> Original
> ![original](original.png)

> Partitioned
> ![partitioned](partitioned.png)

> Reassembled
> ![reassembled](reassembled.png)

