import networkx as nx


def partition(G):
    """Partition a DAG based on a boolean node feature `supported`

    Args:
    G : A networkx directed graph

    Returns: 
    A list of subgraphs

    Note:
    - Input and output nodes have `supported` feature set to None.
    - Auxiliary input and output nodes are created for subgraphs.
    """
    _G = G.copy()
    # First, label all edges in G an edge feature `intra`
    for vs, vt in _G.edges:
        _G[vs][vt]["intra"] = (
            _G.nodes[vs]["supported"] is not None  # source is not an input node
            and _G.nodes[vt]["supported"] is not None  # and target is not an output node
            and not (
                _G.nodes[vs]["supported"] ^ _G.nodes[vt]["supported"]
            )  # and source and target have the same `supported` node feature
        )

    # Next, break non-intra edges, and for each broken, add in a pair of auxiliary input/output nodes/edges for subgraphs
    edges_to_break = [e for e in _G.edges if not _G.edges[e]["intra"]]
    for vs, vt in edges_to_break:
        if not _G[vs][vt]["intra"]:
            _G.remove_edge(vs, vt)  # remove a non-intra edge
            _G.add_nodes_from(
                [
                    ((vs, vt, "in"), {"supported": None}),
                    ((vs, vt, "out"), {"supported": None}),
                ]
            )  # add auxiliary node pair
            _G.add_edges_from(
                [
                    (vs, (vs, vt, "out"), {"intra": True}),
                    ((vs, vt, "in"), vt, {"intra": True}),
                ]
            )  # add auxiliary edge pair

    # Finally, return a list of disjoint subgraphs
    _G_ = nx.subgraph_view(_G, filter_edge=lambda vs, vt: _G[vs][vt]["intra"])
    nbunches = nx.connected_components(_G_.to_undirected())
    return [_G_.subgraph(nb) for nb in nbunches]


def reassemble(SGs):
    """Reassemble a list of sub-DAGs back into an original DAG

    Args: 
    SGs: a list of subgraphs

    Returns: 
    A reassembled graph

    Note:
    - The subgraphs must be disjoint, or an exception will be raised.
    - Matching auxiliary nodes are to be contracted, i.e. node (vs, vt, "out") will identify with node (vs, vt, "in").
    """
    # First, assemble a graph as the disjoint union of all subgraphs
    G = nx.union_all(SGs).copy()

    # Next, unbreak non-intra edges by matching auxiliary nodes
    edges_to_unbreak = [
        v[:2] for v in G.nodes if isinstance(v, tuple) and v[-1] == "out"
    ]
    for vs, vt in edges_to_unbreak:
        G.remove_nodes_from([(vs, vt, "in"), (vs, vt, "out")])  # auxiliary node pair
        G.add_edges_from([(vs, vt, {"intra": False})])  # original edge

    return G