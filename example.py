import networkx as nx
from dag_part import partition, reassemble
from dag_plot import graph_plot


G = nx.DiGraph()
G.add_nodes_from(
    [
        ("in", {"supported": None}),
        ("out", {"supported": None}),
        ("A", {"supported": True}),
        ("B", {"supported": False}),
        ("C", {"supported": True}),
        ("D", {"supported": False}),
        ("E", {"supported": True}),
        ("F", {"supported": True}),
    ]
)
G.add_edges_from(
    [
        ("in", "A"),
        ("A", "B"),
        ("B", "C"),
        ("B", "D"),
        ("C", "F"),
        ("D", "E"),
        ("E", "F"),
        ("F", "out"),
    ]
)

SGs = partition(G)
G_ = reassemble(SGs)

graph_plot(G, "original.png")
graph_plot(SGs, "partitioned.png")
graph_plot(G_, "reassembled.png")
